<?php

namespace eezeecommerce\ImagickBundle\Tests\Mapping;

use eezeecommerce\ImagickBundle\Mapping\UserText;

class UserTextTest extends \PHPUnit_Framework_TestCase
{
    public function testClassExtendsTextEntity()
    {
        $userText = new UserText();

        $this->assertInstanceOf("eezeecommerce\ImagickBundle\Entity\Text", $userText);
    }

    public function testClassCanAppendTextAsAVirtualColumn()
    {
        $userText = new UserText();

        $userText->setText("abc");

        $this->assertEquals("abc", $userText->getText());
    }
}