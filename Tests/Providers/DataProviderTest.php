<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/16
 * Time: 14:52
 */

namespace eezeecommerce\ImagickBundle\Tests\Providers;

use eezeecommerce\ImagickBundle\Entity\Text;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\ImagickBundle\Providers\DataProvider;

class DataProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testDataProviderFunctionality()
    {
        $text = $this->getMock(Text::class);

        $textRepository = $this
            ->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $textRepository->expects($this->once())
            ->method("find")
            ->will($this->returnValue($text));

        $entityManager = $this
            ->getMockBuilder(ObjectManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entityManager->expects($this->once())
            ->method("getRepository")
            ->will($this->returnValue($textRepository));

        $dataProvider = new DataProvider($entityManager);

        $data = $dataProvider->loadByArray(array(1 => "asdasd"));

        $this->assertCount(1, $data);
        $this->assertInstanceOf("eezeecommerce\ImagickBundle\Mapping\UserText", $data[0]);

        $this->assertEquals("asdasd", $data[0]->getText());
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage The Label Id 1 does not exist
     */
    public function testInvalidTextIdWillThrowException()
    {
        $textRepository = $this
            ->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityManager = $this
            ->getMockBuilder(ObjectManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entityManager->expects($this->once())
            ->method("getRepository")
            ->will($this->returnValue($textRepository));

        $dataProvider = new DataProvider($entityManager);

        $dataProvider->loadByArray(array(1 => "asd"));
    }
}