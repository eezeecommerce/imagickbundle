<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/16
 * Time: 15:30
 */

namespace eezeecommerce\ImagickBundle\Tests\Utils;

use eezeecommerce\ImagickBundle\Entity\Attributes;
use eezeecommerce\ImagickBundle\Entity\Image;
use eezeecommerce\ImagickBundle\Mapping\UserText;
use eezeecommerce\ImagickBundle\Utils\Imagick;
use eezeecommerce\ImagickBundle\Entity\Font;
use Symfony\Component\HttpFoundation\File\File;

class ImagickTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage The mapping should not be empty
     */
    public function testClassMappingThrowsErrorWithEmptyArray()
    {
        $draw = $this
            ->getMockBuilder(\ImagickDraw::class)
            ->disableOriginalConstructor()
            ->getMock();

        $imagick = new Imagick($draw);

        $imagick->setMapping(array());
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Mapping array should have instances of UserText only. Instead, array was passed through.
     */
    public function testClassMappingThrowsErrorWithInvalidArray()
    {
        $draw = $this
            ->getMockBuilder(\ImagickDraw::class)
            ->disableOriginalConstructor()
            ->getMock();

        $imagick = new Imagick($draw);

        $imagick->setMapping(array(array("asdasd")));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Mapping array should have instances of UserText only. Instead, instance of stdClass was used.
     */
    public function testClassMappingThrowsErrorWithInvalidInstanceOfObject()
    {
        $draw = $this
            ->getMockBuilder(\ImagickDraw::class)
            ->disableOriginalConstructor()
            ->getMock();

        $imagick = new Imagick($draw);

        $imagick->setMapping(array(new \stdClass()));
    }

    public function testClassSetMethodsAreCalled()
    {
        $userText = new UserText();

        $imagick = $this
            ->getMockBuilder(Imagick::class)
            ->disableOriginalConstructor()
            ->setMethods(array("setText"))
            ->getMock();

        $imagick->expects($this->once())
            ->method("setText")
            ->with($this->equalTo($userText));

        $imagick->setMapping(array($userText));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Attributes cannot be null
     */
    public function testClassSetTextNullAttributesThrowsError()
    {
        $attributes = null;
        $userText = new UserText();

        $imagick = $this
            ->getMockBuilder(Imagick::class)
            ->disableOriginalConstructor()
            ->setMethods(array("setAttributes"))
            ->getMock();

        $imagick->expects($this->never())
            ->method("setAttributes")
            ->with($this->equalTo($attributes));

        $imagick->setMapping(array($userText));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Font cannot be null
     */
    public function testClassSetTextNullFontThrowsError()
    {
        $attributes = new Attributes();
        $userText = new UserText();
        $userText->setAttributes($attributes);

        $imagick = $this
            ->getMockBuilder(Imagick::class)
            ->disableOriginalConstructor()
            ->setMethods(array("setFont"))
            ->getMock();

        $imagick->expects($this->never())
            ->method("setFont")
            ->with($this->equalTo($attributes));

        $imagick->setMapping(array($userText));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Imagick should be instance of Imagick. Instead, instance of eezeecommerce\ImagickBundle\Utils\Imagick was passed through
     */
    public function testClassSetTextAttributesFontCallsSetters()
    {
        $attributes = new Attributes();
        $font = new Font();
        $userText = new UserText();
        $userText->setAttributes($attributes);
        $userText->setFont($font);

        $imagick = $this
            ->getMockBuilder(Imagick::class)
            ->disableOriginalConstructor()
            ->setMethods(array("setFont", "setAttributes"))
            ->getMock();

        $imagick->expects($this->once())
            ->method("setFont")
            ->with($this->equalTo($font));

        $imagick->expects($this->once())
            ->method("setAttributes")
            ->with($this->equalTo($attributes));

        $imagick->setMapping(array($userText));
    }


    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Imagick should be instance of Imagick. Instead, instance of eezeecommerce\ImagickBundle\Utils\Imagick was passed through
     */
    public function testClassMethodDrawSucceeds()
    {
        $draw = $this
            ->getMockBuilder("\ImagickDraw")
            ->disableOriginalConstructor()
            ->getMock();

        $imagick = new Imagick($draw);

        $attributes = new Attributes();
        $font = new Font();
        $userText = new UserText();
        $userText->setAttributes($attributes);
        $userText->setFont($font);

        $imagick->setMapping(array($userText));

        $imagick->write();
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid Image has been used
     */
    public function testClassMethodSetImageThrowsException()
    {
        $draw = $this
            ->getMockBuilder("\ImagickDraw")
            ->disableOriginalConstructor()
            ->getMock();

        $image = new Image();

        $imagick = new Imagick($draw);

        $imagick->setImage($image);
    }

    public function testClassWontProcessInvalidImage()
    {
        $attributes = new Attributes();

        $file = new File(__DIR__."/../Data/test.ttf");
        $font = new Font();
        $font->setFontFile($file);
        $userText = new UserText();
        $userText->setAttributes($attributes);
        $userText->setFont($font);

        $draw = $this
            ->getMockBuilder("\ImagickDraw")
            ->disableOriginalConstructor()
            ->getMock();

        $imagick = new Imagick($draw);

        $file = new File(__DIR__."/../Data/logo_symfony.png");

        $image = new Image();
        $image->setImageFile($file);

        $imagick->setImage($image);

        $imagick->setMapping(array($userText));

        $imagick->write();
    }

}