<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/16
 * Time: 14:36
 */

namespace eezeecommerce\ImagickBundle\Mapping;


use eezeecommerce\ImagickBundle\Entity\Text;

class UserText extends Text
{
    private $text;

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getText()
    {
        return $this->text;
    }
}