<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/03/16
 * Time: 14:44
 */

namespace eezeecommerce\ImagickBundle\Providers;


use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\ImagickBundle\Mapping\UserText;

class DataProvider
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var array
     */
    private $mapping = array();

    /**
     * DataProvider constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param array $array
     * @return array
     */
    public function loadByArray(array $array)
    {
        foreach ($array as $id => $text) {
            $this->createMapping($id, $text);
        }

        return $this->mapping;
    }

    /**
     * @param $id
     * @param $text
     *
     * @return void
     */
    protected function createMapping($id, $text)
    {
        $entity = $this->em->getRepository("eezeecommerceImagickBundle:Text")
            ->find($id);

        if (null === $entity) {
            throw new \InvalidArgumentException(sprintf("The Label Id %s does not exist", $id));
        }

        $mapping = new UserText($entity);
        $mapping->setText($text);

        $this->mapping[] = $mapping;

    }
}