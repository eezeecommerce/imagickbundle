<?php

namespace eezeecommerce\ImagickBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @codeCoverageIgnore
 */
class Text
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Orm\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $x;

    /**
     * @ORM\Column(type="integer")
     */
    private $y;

    /**
     * @ORM\Column(type="integer")
     */
    private $angle = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $max_chars;

    /**
     * @ORM\OneToOne(targetEntity="Attributes")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     */
    private $attributes;

    /**
     * @ORM\OneToOne(targetEntity="Font")
     * @ORM\JoinColumn(name="font_id", referencedColumnName="id")
     */
    private $font;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return Text
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return integer 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     * @return Text
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return integer 
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set angle
     *
     * @param integer $angle
     * @return Text
     */
    public function setAngle($angle)
    {
        $this->angle = $angle;

        return $this;
    }

    /**
     * Get angle
     *
     * @return integer 
     */
    public function getAngle()
    {
        return $this->angle;
    }

    /**
     * Set max_chars
     *
     * @param integer $maxChars
     * @return Text
     */
    public function setMaxChars($maxChars)
    {
        $this->max_chars = $maxChars;

        return $this;
    }

    /**
     * Get max_chars
     *
     * @return integer 
     */
    public function getMaxChars()
    {
        return $this->max_chars;
    }

    /**
     * Set attributes
     *
     * @param \eezeecommerce\ImagickBundle\Entity\Attributes $attributes
     * @return Text
     */
    public function setAttributes(\eezeecommerce\ImagickBundle\Entity\Attributes $attributes = null)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get attributes
     *
     * @return \eezeecommerce\ImagickBundle\Entity\Attributes 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set font
     *
     * @param \eezeecommerce\ImagickBundle\Entity\Font $font
     * @return Text
     */
    public function setFont(\eezeecommerce\ImagickBundle\Entity\Font $font = null)
    {
        $this->font = $font;

        return $this;
    }

    /**
     * Get font
     *
     * @return \eezeecommerce\ImagickBundle\Entity\Font 
     */
    public function getFont()
    {
        return $this->font;
    }
}
