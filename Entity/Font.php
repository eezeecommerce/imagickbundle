<?php

namespace eezeecommerce\ImagickBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @codeCoverageIgnore
 * @Vich\Uploadable
 */
class Font
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Orm\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="label_font", fileNameProperty="fontName")
     *
     * @var File
     */
    private $fontFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $fontName;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $font
     *
     * @return Font
     */
    public function setFontFile(File $font = null)
    {
        $this->fontFile = $font;

        if ($font) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getFontFile()
    {
        return $this->fontFile;
    }

    /**
     * @param string $imageName
     *
     * @return Font
     */
    public function setImageName($fontName)
    {
        $this->fontName = $fontName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFontName()
    {
        return $this->fontName;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fontName
     *
     * @param string $fontName
     *
     * @return Font
     */
    public function setFontName($fontName)
    {
        $this->fontName = $fontName;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Font
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
