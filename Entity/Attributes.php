<?php

namespace eezeecommerce\ImagickBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @codeCoverageIgnore
 */
class Attributes
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Orm\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $fontSize;

    /**
     * @ORM\Column(type="string")
     */
    private $colour;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fontSize
     *
     * @param integer $fontSize
     * @return Attributes
     */
    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;

        return $this;
    }

    /**
     * Get fontSize
     *
     * @return integer 
     */
    public function getFontSize()
    {
        return $this->fontSize;
    }

    /**
     * Set colour
     *
     * @param string $colour
     * @return Attributes
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour
     *
     * @return string 
     */
    public function getColour()
    {
        return $this->colour;
    }
}
