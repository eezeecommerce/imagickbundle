<?php

namespace eezeecommerce\ImagickBundle\Utils;

use eezeecommerce\ImagickBundle\Entity\Attributes;
use eezeecommerce\ImagickBundle\Entity\Font;
use eezeecommerce\ImagickBundle\Entity\Image;
use eezeecommerce\ImagickBundle\Mapping\UserText;


class Imagick
{
    /**
     * @var int
     */
    public static $center = 2;

    /**
     * @var \Imagick
     */
    private $imagick;

    /**
     * @var \ImagickDraw
     */
    private $draw;

    /**
     * @var UserText
     */
    private $text;

    /**
     * @var Attributes
     */
    private $attributes;

    /**
     * @var Font
     */
    private $font;

    public function __construct(\ImagickDraw $draw)
    {
        $this->draw = $draw;
    }

    public function setImage(Image $image)
    {
        if (null == $image->getImageFile()) {
            throw new \InvalidArgumentException("Invalid Image has been used");
        }

        $this->imagick = new \Imagick($image->getImageFile()->getRealPath());
    }

    public function setMapping(array $mapping)
    {
        if (empty($mapping)) {
            throw new \InvalidArgumentException("The mapping should not be empty");
        }

        foreach ($mapping as $class) {
            if (!$class instanceof UserText) {
                if (is_object($class)) {
                    throw new \InvalidArgumentException(sprintf("Mapping array should have instances of UserText only. Instead, instance of %s was used.", get_class($class)));
                }
                throw new \InvalidArgumentException(sprintf("Mapping array should have instances of UserText only. Instead, %s was passed through.", gettype($class)));
            }
            $this->setText($class);
        }
    }

    public function setText(UserText $text)
    {
        $this->text = $text;
        if (null === $text->getAttributes()) {
            throw new \InvalidArgumentException("Attributes cannot be null");
        }

        if (null === $text->getFont()) {
            throw new \InvalidArgumentException("Font cannot be null");
        }

        $this->setAttributes($text->getAttributes());
        $this->setFont($text->getFont());
        $this->write();
    }

    public function setAttributes(Attributes $attributes)
    {
        $this->attributes = $attributes;
    }

    public function setFont(Font $font)
    {
        $this->font = $font;
    }

    /**
     * Write data to Image
     *
     * return void
     */
    public function write()
    {
        if (!$this->imagick instanceof \Imagick) {
            throw new \InvalidArgumentException(sprintf("Imagick should be instance of Imagick. Instead, instance of %s was passed through", get_class($this->imagick)));
        }

        $this->draw->setFont($this->font->getFontFile()->getRealPath());
        $this->draw->setFontSize($this->attributes->getFontSize());
        $this->draw->setFillColor($this->attributes->getColour());
        $this->draw->setTextAlignment(self::$center);
        $this->draw->setTextEncoding("utf-8");
        $this->draw->setStrokeAntialias(true);
        $this->draw->setTextAntialias(true);

        $dimensions = $this->imagick->getImageGeometry();

        if (!array_key_exists("width", $dimensions)) {
            throw new \InvalidArgumentException("The supplied image doesn't have a width. Please try a different image");
        }

        if (!array_key_exists("height", $dimensions)) {
            throw new \InvalidArgumentException("The supplied image doesn't have a height. Please try a different image");
        }

        $x = $this->getWidth($dimensions["width"]);
        $y = $this->getHeight($dimensions["height"]);

        $this->imagick->annotateImage($this->draw, $x, $y, $this->text->getAngle(), $this->text->getText());
    }

    /**
     * @param $width
     * @return float
     */
    private function getWidth($width)
    {
        return ($width / self::$center) + $this->text->getX();
    }

    /**
     * @param $height
     * @return int
     */
    private function getHeight($height)
    {
        return $this->text->getY();
    }
}