Imagick Bundle
==============

### AppKernel

``` php

    public function registerBundles()
    {
        $bundles = array(
        // ...
            new Vich\UploaderBundle\VichUploaderBundle(),
        // ...
        );
    }

```

### Config.yml

``` yaml

# app/config/config.yml

vich_uploader:
    db_driver: orm
    mappings:
        label_image:
            upload_destination: %kernel.root_dir%/../web/images/label_images
            uri_prefix: /images/label_images
            namer:  vich_uploader.namer_origname
        label_fonts:
                    upload_destination: %kernel.root_dir%/../web/images/label_fonts
                    uri_prefix: /images/label_fonts
                    namer:  vich_uploader.namer_origname

```
